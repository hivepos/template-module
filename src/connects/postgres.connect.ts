import fs from "fs";
import path from "path";
import { Sequelize, Options } from "sequelize";
const { DB_URL, FILETYPE, NODE_ENV } = process.env;

const basename = path.basename(__filename);
const db: any = {};
const options: Options = {
  dialect: "postgres",
  logging: NODE_ENV === "dev" ? console.log : false
};
const isTsFile = (file: any) => {
  return (
    file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === FILETYPE
  );
};

db.sequelize = new Sequelize(DB_URL, options);
fs.readdirSync(path.join(__dirname, "../domains"))
  .filter(isTsFile)
  .map(file => {
    const model = db.sequelize.import(path.join(__dirname, "../domains", file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

export default db;
