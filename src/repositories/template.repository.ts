import postgresDB from "../connects/postgres.connect";

export const findAllTemplates = async () => {
  return await postgresDB.templates.findAll();
};
