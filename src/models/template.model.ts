import { UUID, fn } from "sequelize";

export default {
  id: {
    allowNull: false,
    primaryKey: true,
    type: UUID
  },
  created_at: {
    defaultValue: fn("NOW"),
    type: "TIMESTAMP"
  },
  updated_at: {
    defaultValue: fn("NOW"),
    type: "TIMESTAMP"
  }
};
