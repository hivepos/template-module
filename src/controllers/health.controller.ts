import express, { Response } from "express";
import { IRequest } from "../interfaces/express";
const healthRouter = express.Router();

healthRouter.get("/", (_req: IRequest, res: Response) => {
  return res.status(200).send("OK");
});

export default healthRouter;
