import { Sequelize } from "sequelize";
import templateModel from "../models/template.model";

export default (sequelize: Sequelize) => {
  return sequelize.define("templates", templateModel, { timestamps: false });
};
