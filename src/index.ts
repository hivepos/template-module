import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import { createServer } from "http";
import HealthController from "./controllers/health.controller";
import Services from "./services";
import Repositories from "./repositories";
const { PORT } = process.env;

const app = express();
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/ping", HealthController);
app.use("/templates", async (req, res) => {
  const allTemplates = await Repositories.Template.findAllTemplates();
  return res.status(200).send({ allTemplates });
});
app.use("*", (req, res) => res.redirect("/ping"));

const server = createServer(app);
server.listen(PORT, Services.Log.introduceServer);
