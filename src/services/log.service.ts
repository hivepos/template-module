const {
  PORT,
  HOST,
  HTTP_PROTOCOL,
  HTTP_PATH
} = process.env;

export const introduceServer = () => {
  const httpUrl = `${HTTP_PROTOCOL}://${HOST}/${HTTP_PATH}`;
  const log = `🙈 Listening http on port: ${PORT}\n🙊 Http link portal: ${httpUrl}`;
  console.info(log);
};
